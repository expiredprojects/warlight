from sys import stderr, stdin, stdout
from random import randrange

import os


class Config(object):
    Name = 'Dave'
    Log = '../logs/[%s]logs.txt' % Name
    Error = '../logs/[%s]errors.txt' % Name
    Logging = False


class Strategy(object):
    Timing = 0
    Aggressive = 0.2
    MidAggressive = 0.35
    Balanced = 0.5
    MidDefensive = 0.65
    Defensive = 0.8
    VS_BALANCED = [
        (0, 20, Balanced),
        (20, 40, MidAggressive),
        (40, 60, MidDefensive),
        (60, 80, MidAggressive),
        (80, 100, MidDefensive)
    ]
    VS_AGGRESSIVE = [
        (0, 20, Defensive),
        (20, 40, MidDefensive),
        (40, 60, Balanced),
        (60, 80, MidAggressive),
        (80, 100, MidAggressive)
    ]
    VS_DEFENSIVE = [
        (0, 20, Balanced),
        (20, 40, Balanced),
        (40, 60, Balanced),
        (60, 80, MidAggressive),
        (80, 100, MidAggressive)
    ]
    Behaviour = VS_DEFENSIVE

    @staticmethod
    def next_round():
        Strategy.Timing += 1
        return Strategy.Timing

    def parse_behaviour(self):
        now = self.Timing
        for behaviour in self.Behaviour:
            if behaviour[0] <= now <= behaviour[1]:
                return behaviour[2]
        return self.Balanced

    def resolve(self, arg):
        raise NotImplementedError


class PickupExpert(Strategy):
    def resolve(self, options):
        return self.dummy(options)

    @staticmethod
    def dummy(options):
        options = Random.shuffle(options)
        return options[6:12]


class DeployExpert(Strategy):
    def resolve(self, bot):
        troops_remaining = int(bot.settings['starting_armies'])
        player_regions = bot.map.get_owned_regions(bot.settings['your_bot'])
        enemy_regions = bot.map.get_owned_regions(bot.settings['opponent_bot'])
        return self.dummy(troops_remaining, player_regions, enemy_regions)

    def dummy(self, troops_remaining, player_regions, enemy_regions):
        # with open(Config.Log, 'a+') as f:
            #f.write('\n-------- New round: troops %s\n' % troops_remaining)
            placements = list()
            used = list()
            troops_total = troops_remaining
            if len(player_regions) <= 1:
                #f.write('Assign to one: troops %s, regions %s\n' % (troops_total, player_regions))
                return [0, troops_remaining]
            while troops_remaining:
                minimal_attacking = round(troops_total * self.parse_behaviour())
                #f.write('Troops: %s, minimal attacking %s\n' % (troops_remaining, minimal_attacking))
                if troops_remaining > minimal_attacking:
                    region = self.find_nearby(player_regions, enemy_regions, used)
                    #f.write('Region after nearby: %s\n' % region)
                if not region:
                    region = self.find_random(player_regions, used)
                    #f.write('Region after random: %s\n' % region)
                if troops_remaining > 1:
                    #f.write('Savepoint\n')
                    amount = self.get_amount(troops_remaining, used)
                    #f.write('Amount picked: %s\n' % amount)
                else:
                    amount = 1
                    #f.write('Amount forced to 1\n')
                #f.write('Compare: region: %s, used %s\n' % (region, used))
                if region not in used:
                    used.append(region)
                    placements.append([region.id, amount])
                    #f.write('Amount %s to region %s\n' % (amount, region))
                else:
                    indexes = [p[0] for p in placements]
                    index = indexes.index(region.id)
                    placements[index][1] += amount
                    #f.write('Amount %s to existing %s\n' % (amount, region))
                region.troop_count += amount
                troops_remaining -= amount
                #f.write('After region troops: %s, remaining %s\n' % (region.troop_count, troops_remaining))
            #f.write('Deploy finished')
            return placements

    @staticmethod
    def find_nearby(player_regions, enemy_regions, used):
        player_set = set(player_regions)
        for enemy_region in enemy_regions:
            targets = list(player_set.intersection(enemy_region.neighbours))
            targets = list(set(targets).difference(used))
            if targets:
                return targets[0]
        return None

    @staticmethod
    def find_random(player_regions, used):
        free_regions = list(set(player_regions).difference(used))
        if free_regions:
            return Random.shuffle(free_regions)[0]
        return Random.shuffle(player_regions)[0]

    @staticmethod
    def get_amount(troops_remaining, used):
        ratio = 0.5 * len(used)
        amount = round(troops_remaining * ratio)
        if amount > 0:
            return amount
        amount = round(troops_remaining * 0.5)
        return amount if amount > 0 else troops_remaining


class AttackExpert(Strategy):
    def resolve(self, bot):
        player_regions = bot.map.get_owned_regions(bot.settings['your_bot'])
        enemy_regions = bot.map.get_owned_regions(bot.settings['opponent_bot'])
        return self.base(player_regions, enemy_regions)

    def dummy(self, player_regions, enemy_regions):
        attack_transfers = []
        for region in player_regions:
            target_region = self.process_region(region, enemy_regions)
            if target_region:
                amount = region.troop_count - 1
                attack_transfers.append([region.id, target_region.id, amount])
                region.troop_count -= amount
        return attack_transfers

    @staticmethod
    def reduce(l):
        return [item for sub_list in l for item in sub_list]

    def process_neighbours(self, neighbours, enemy_regions, depth=1):
        if depth < 1:
            return None
        for n in neighbours:
            target = self.attack_nearby(n.neighbours, enemy_regions)
            if target:
                return target
        for n in neighbours:
            target = self.process_neighbours(n.neighbours, enemy_regions, depth-1)
            if target:
                return target
        return None

    def process_region(self, region, enemy_regions):
        if region.troop_count == 1:
            return None
        neighbours = region.neighbours
        target = self.attack_nearby(neighbours, enemy_regions)
        if not target:
            target = self.process_neighbours(neighbours, enemy_regions)
        if not target:
            target = self.transfer_random(neighbours)
        return target

    def base(self, player_regions, enemy_regions):
        #with open(Config.Log, 'a+') as f:
            f = None
            #f.write('\n-------- New attack\n')
            attack_transfers = []
            for region in player_regions:
                neighbours = list(region.neighbours)
                while len(neighbours) > 0:
                    target_region = None
                    if Random.xrandrange(0, 1) > self.parse_behaviour():
                        target_region = self.attack_nearby(neighbours, enemy_regions)
                        #f.write('Attack nearby: %s\n' % target_region)
                    if not target_region:
                        if Random.xrandrange(0.5, 1) > self.parse_behaviour():
                            target_region = self.attack_close(neighbours, enemy_regions)
                            #f.write('Attack close: %s\n' % target_region)
                        if not target_region:
                            target_region = self.attack_backup(f, neighbours, player_regions, enemy_regions)
                            #f.write('Attack backup: %s\n' % target_region)
                            if not target_region:
                                target_region = self.attack_neutral(neighbours)
                                #f.write('Attack neutral: %s\n' % target_region)
                                if not target_region:
                                    target_region = self.transfer_random(neighbours)
                                    #f.write('Attack random: %s\n' % target_region)
                    #f.write('Region details: %s\n' % target_region)
                    different_owner = region.owner != target_region.owner
                    ratio = 1 + self.parse_behaviour()
                    attack_group = round(target_region.troop_count * ratio)
                    advantage = region.troop_count >= attack_group
                    #f.write('Is enemy: %s\n' % target_region)
                    #f.write('Ratio: %s\n' % ratio)
                    #f.write('Desired attack: %s\n' % attack_group)
                    #f.write('Resources: %s\n' % region.troop_count)
                    if different_owner and advantage:
                        attack_transfers.append([region.id, target_region.id, int(attack_group)])
                        region.troop_count -= attack_group
                        #f.write('Attack with attack group\n')
                    elif not different_owner and region.troop_count > 1:
                        attack_transfers.append([region.id, target_region.id, int(region.troop_count - 1)])
                        region.troop_count = 1
                        #f.write('Transfer to reinforces\n')
                    neighbours.remove(target_region)
            #f.write('Close attack phase\n')
            #f.write('Dump transfers: %s\n' % attack_transfers)
            return attack_transfers

    def legacy(self, player_regions, enemy_regions):
        attack_transfers = []
        for region in player_regions:
            neighbours = list(region.neighbours)
            while len(neighbours) > 0:
                target_region = self.attack_nearby(neighbours, enemy_regions)
                if not target_region:
                    target_region = self.attack_close(neighbours, enemy_regions)
                    if not target_region:
                        # target_region = self.attack_backup(neighbours, player_regions, enemy_regions)
                        if not target_region:
                            target_region = self.attack_neutral(neighbours)
                            if not target_region:
                                target_region = self.transfer_random(neighbours)
                if region.owner != target_region.owner and region.troop_count > 6:
                    attack_transfers.append([region.id, target_region.id, 5])
                    region.troop_count -= 5
                elif region.owner == target_region.owner and region.troop_count > 1:
                    attack_transfers.append([region.id, target_region.id, int(region.troop_count - 1)])
                    region.troop_count = 1
                else:
                    neighbours.remove(target_region)
        return attack_transfers

    @staticmethod
    def attack_nearby(neighbours, enemy_regions):
        enemy = list(set(neighbours).intersection(enemy_regions))
        return enemy[0] if enemy else None

    @staticmethod
    def attack_close(neighbours, enemy_regions):
        neighbours_set = set(neighbours)
        for enemy_region in enemy_regions:
            targets = list(neighbours_set.intersection(enemy_region.neighbours))
            if targets:
                return targets[0]
        return None

    @staticmethod
    def attack_neutral(neighbours):
        neutral = [neighbour for neighbour in neighbours if neighbour.owner == 'neutral']
        return neutral[0] if neutral else None

    @staticmethod
    def flatten(l):
        return [item for sublist in l for item in sublist]

    def attack_backup(self, f, neighbours, player_regions, enemy_regions):
        #f.write('Init\n')
        my_n_set = set(neighbours)
        #f.write('My N %s\n' % my_n_set)
        nested_enemies = [enemy_region.neighbours for enemy_region in enemy_regions]
        #f.write('Enemies Nested %s\n' % nested_enemies)
        enemy_n_set = set(self.flatten(nested_enemies))
        #f.write('Enemies Flat %s\n' % enemy_n_set)
        dangers = list(enemy_n_set.intersection(player_regions))
        #f.write('Dangers N %s\n' % dangers)
        for danger in dangers:
            danger_n = danger.neighbours
            close_backup = list(my_n_set.intersection(danger_n))
            if close_backup:
                return close_backup[0]
        return None

    @staticmethod
    def transfer_random(neighbours):
        return neighbours[int(Random.xrandrange(0, len(neighbours)))]


class State(object):
    settings = 'settings'
    setup_map = 'setup_map'
    setup_map_super_regions = 'super_regions'
    setup_map_regions = 'regions'
    setup_map_neighbours = 'neighbors'
    update_map = 'update_map'
    pick_starting_regions = 'pick_starting_regions'
    opponent_moves = 'opponent_moves'
    go = 'go'
    go_place_armies = 'place_armies'
    go_attack_transfer = 'attack/transfer'


class Bot(object):
    def __init__(self):
        self.settings = dict()
        self.map = Map()
        self.pickup = PickupExpert()
        self.deploy = DeployExpert()
        self.attack = AttackExpert()

    def run(self):
        while not stdin.closed:
            try:
                # read
                raw_line = stdin.readline()
                if len(raw_line) == 0:
                    break

                # strip
                line = raw_line.strip()
                if len(line) == 0:
                    continue

                # split
                parts = line.split()
                command = parts[0]

                # evaluate
                if command == State.settings:
                    self.update_settings(parts[1:])
                elif command == State.setup_map:
                    self.setup_map(parts[1:])
                elif command == State.update_map:
                    self.update_map(parts[1:])
                elif command == State.pick_starting_regions:
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()
                elif command == State.go:
                    sub_command = parts[1]
                    if sub_command == State.go_place_armies:
                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()
                    elif sub_command == State.go_attack_transfer:
                        result = self.attack_transfer()
                        stdout.write(result + '\n')
                        stdout.flush()
                        #with open(Config.Log, 'a+') as f:
                            #f.write('!!!!!!!!!!!!\n')
                            #f.write(result + '\n')
                    else:
                        stderr.write('Unknown sub command: %s\n' % sub_command)
                        stderr.flush()
                elif command == State.opponent_moves:
                    pass
                else:
                    stderr.write('Unknown command: %s\n' % command)
                    stderr.flush()
            except EOFError:
                return
            #except Exception as e:
            #    with open(Config.Error, 'a+') as f:
                    #f.write(e.__str__())
                    #f.write('\n')

    def update_settings(self, options):
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        map_type = options[0]
        for i in range(1, len(options), 2):
            if map_type == State.setup_map_super_regions:
                self.setup_super_regions(options, i)
            elif map_type == State.setup_map_regions:
                self.setup_map_regions(options, i)
            elif map_type == State.setup_map_neighbours:
                self.setup_map_neighbours(options, i)
        if map_type == State.setup_map_neighbours:
            self.apply_map_neighbours()

    def setup_super_regions(self, options, i):
        super_region = SuperRegion(options[i], int(options[i + 1]))
        self.map.super_regions.append(super_region)

    def setup_map_regions(self, options, i):
        super_region = self.map.get_super_region_by_id(options[i + 1])
        region = Region(options[i], super_region)
        self.map.regions.append(region)
        super_region.regions.append(region)

    def setup_map_neighbours(self, options, i):
        region = self.map.get_region_by_id(options[i])
        neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]
        for neighbour in neighbours:
            region.neighbours.append(neighbour)
            neighbour.neighbours.append(region)

    def apply_map_neighbours(self):
        for region in self.map.regions:
            if region.is_on_super_region_border:
                continue
            for neighbour in region.neighbours:
                if neighbour.super_region.id != region.super_region.id:
                    region.is_on_super_region_border = True
                    neighbour.is_on_super_region_border = True

    def update_map(self, options):
        for i in range(0, len(options), 3):
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])
        Strategy.next_round()

    def pick_starting_regions(self, options):
        picked = self.pickup.resolve(options)
        return ' '.join(picked)

    def place_troops(self):
        placements = self.deploy.resolve(self)
        return ', '.join(['%s %s %s %d' % (
            self.settings['your_bot'],
            State.go_place_armies,
            placement[0],
            placement[1]
        ) for placement in placements])

    def attack_transfer(self):
        attack_transfers = self.attack.resolve(self)
        if len(attack_transfers) == 0:
            return 'No moves'
        return ', '.join(['%s %s %s %s %s' % (
            self.settings['your_bot'],
            State.go_attack_transfer,
            attack_transfer[0],
            attack_transfer[1],
            attack_transfer[2]
        ) for attack_transfer in attack_transfers])


class Random(object):
    @staticmethod
    def xrandrange(_min, _max):
        res = float(_min) + randrange(0, 100)/100.0 * float(_max - _min)
        return res

    @staticmethod
    def shuffle(items):
        i = len(items)
        while i > 1:
            i -= 1
            j = int(Random.xrandrange(0, i))
            items[j], items[i] = items[i], items[j]
        return items


class Region(object):
    def __init__(self, region_id, super_region):
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False


class SuperRegion(object):
    def __init__(self, super_region_id, worth):
        self.id = super_region_id
        self.worth = worth
        self.regions = []


class Map(object):
    def __init__(self):
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        return [region for region in self.regions if region.id == region_id][0]

    def get_super_region_by_id(self, super_region_id):
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        return [region for region in self.regions if region.owner == owner]


def reset_logs(_file):
    if os.path.exists(_file):
        os.remove(_file)
        return True
    else:
        return False


if __name__ == '__main__':
    reset_logs(Config.Log)
    # reset_logs(Config.Error)
    Bot().run()
