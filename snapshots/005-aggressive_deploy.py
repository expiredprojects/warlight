from math import fmod, pi
from sys import stderr, stdin, stdout
from time import clock


class Strategy(object):
    Aggressive = 0.2
    Defensive = 0.8
    Balanced = 0.5
    Current = Aggressive

    def resolve(self, arg):
        raise NotImplementedError


class PickupExpert(Strategy):
    def resolve(self, options):
        return self.dummy(options)

    @staticmethod
    def dummy(options):
        # return Random.shuffle(options)[:6]
        return options[6:12]


class DeployExpert(Strategy):
    def resolve(self, bot):
        troops_remaining = int(bot.settings['starting_armies'])
        player_regions = bot.map.get_owned_regions(bot.settings['your_bot'])
        enemy_regions = bot.map.get_owned_regions(bot.settings['opponent_bot'])
        return self.dummy(troops_remaining, player_regions, enemy_regions)

    def dummy(self, troops_remaining, player_regions, enemy_regions):
        placements = list()
        used = list()
        troops_total = troops_remaining
        while troops_remaining:
            if troops_remaining > round(troops_total * self.Current):
                region = self.find_nearby(player_regions, enemy_regions, used)
            if not region:
                region = self.find_random(player_regions, used)
            if troops_remaining > 1:
                amount = self.get_amount(troops_remaining, used)
            else:
                amount = 1
            if region not in used:
                used.append(region)
                placements.append([region.id, amount])
            else:
                indexes = [p[0] for p in placements]
                index = indexes.index(region.id)
                placements[index][1] += amount
            region.troop_count += amount
            troops_remaining -= amount
        return placements

    @staticmethod
    def find_nearby(player_regions, enemy_regions, used):
        player_set = set(player_regions)
        for enemy_region in enemy_regions:
            targets = list(player_set.intersection(enemy_region.neighbours))
            targets = list(set(targets).difference(used))
            if targets:
                return targets[0]
        return None

    @staticmethod
    def find_random(player_regions, used):
        free_regions = list(set(player_regions).difference(used))
        if free_regions:
            return Random.shuffle(free_regions)[0]
        return Random.shuffle(player_regions)[0]

    @staticmethod
    def get_amount(troops_remaining, used):
        ratio = 0.5 - len(used)
        amount = round(troops_remaining * ratio)
        if amount > 0:
            return amount
        amount = round(troops_remaining * 0.5)
        return amount if amount > 0 else troops_remaining


class AttackExpert(Strategy):
    def resolve(self, bot):
        player_regions = bot.map.get_owned_regions(bot.settings['your_bot'])
        enemy_regions = bot.map.get_owned_regions(bot.settings['opponent_bot'])
        return self.base(player_regions, enemy_regions)

    def dummy(self, player_regions, enemy_regions):
        attack_transfers = []
        for region in player_regions:
            target_region = self.process_region(region, enemy_regions)
            if target_region:
                amount = region.troop_count - 1
                attack_transfers.append([region.id, target_region.id, amount])
                region.troop_count -= amount
        return attack_transfers

    @staticmethod
    def reduce(l):
        return [item for sub_list in l for item in sub_list]

    def process_neighbours(self, neighbours, enemy_regions, depth=1):
        if depth < 1:
            return None
        for n in neighbours:
            target = self.attack_nearby(n.neighbours, enemy_regions)
            if target:
                return target
        for n in neighbours:
            target = self.process_neighbours(n.neighbours, enemy_regions, depth-1)
            if target:
                return target
        return None

    def process_region(self, region, enemy_regions):
        if region.troop_count == 1:
            return None
        neighbours = region.neighbours
        target = self.attack_nearby(neighbours, enemy_regions)
        if not target:
            target = self.process_neighbours(neighbours, enemy_regions)
        if not target:
            target = self.transfer_random(neighbours)
        return target

    def base(self, player_regions, enemy_regions):
        attack_transfers = []
        for region in player_regions:
            neighbours = list(region.neighbours)
            while len(neighbours) > 0:
                target_region = self.attack_nearby(neighbours, enemy_regions)
                if not target_region:
                    target_region = self.attack_close(neighbours, enemy_regions)
                    if not target_region:
                        # target_region = self.attack_backup(neighbours, player_regions, enemy_regions)
                        if not target_region:
                            target_region = self.attack_neutral(neighbours)
                            if not target_region:
                                target_region = self.transfer_random(neighbours)
                if region.owner != target_region.owner and region.troop_count > 6:
                    attack_transfers.append([region.id, target_region.id, 5])
                    region.troop_count -= 5
                elif region.owner == target_region.owner and region.troop_count > 1:
                    attack_transfers.append([region.id, target_region.id, region.troop_count - 1])
                    region.troop_count = 1
                else:
                    neighbours.remove(target_region)
        return attack_transfers

    @staticmethod
    def attack_nearby(neighbours, enemy_regions):
        enemy = list(set(neighbours).intersection(enemy_regions))
        return enemy[0] if enemy else None

    @staticmethod
    def attack_close(neighbours, enemy_regions):
        neighbours_set = set(neighbours)
        for enemy_region in enemy_regions:
            targets = list(neighbours_set.intersection(enemy_region.neighbours))
            if targets:
                return targets[0]
        return None

    @staticmethod
    def attack_neutral(neighbours):
        neutral = [neighbour for neighbour in neighbours if neighbour.owner == 'neutral']
        return neutral[0] if neutral else None

    @staticmethod
    def attack_backup(neighbours, player_regions, enemy_regions):
        neighbours_set = set(neighbours)
        for enemy_region in enemy_regions:
            dangers = list(set(enemy_region).intersection(player_regions))
            for danger in dangers:
                close_backup = neighbours_set.intersection(danger.neighbours)
                if close_backup:
                    return close_backup[0]
        return None

    @staticmethod
    def transfer_random(neighbours):
        return neighbours[Random.randrange(0, len(neighbours))]


class State(object):
    settings = 'settings'
    setup_map = 'setup_map'
    setup_map_super_regions = 'super_regions'
    setup_map_regions = 'regions'
    setup_map_neighbours = 'neighbors'
    update_map = 'update_map'
    pick_starting_regions = 'pick_starting_regions'
    opponent_moves = 'opponent_moves'
    go = 'go'
    go_place_armies = 'place_armies'
    go_attack_transfer = 'attack/transfer'


class Bot(object):
    def __init__(self):
        self.settings = dict()
        self.map = Map()
        self.pickup = PickupExpert()
        self.deploy = DeployExpert()
        self.attack = AttackExpert()
        self.round = 0

    def run(self):
        while not stdin.closed:
            try:
                # read
                raw_line = stdin.readline()
                if len(raw_line) == 0:
                    break

                # strip
                line = raw_line.strip()
                if len(line) == 0:
                    continue

                # split
                parts = line.split()
                command = parts[0]

                # evaluate
                if command == State.settings:
                    self.update_settings(parts[1:])
                elif command == State.setup_map:
                    self.setup_map(parts[1:])
                elif command == State.update_map:
                    self.update_map(parts[1:])
                elif command == State.pick_starting_regions:
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()
                elif command == State.go:
                    sub_command = parts[1]
                    if sub_command == State.go_place_armies:
                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()
                    elif sub_command == State.go_attack_transfer:
                        stdout.write(self.attack_transfer() + '\n')
                        stdout.flush()
                    else:
                        stderr.write('Unknown sub command: %s\n' % sub_command)
                        stderr.flush()
                elif command == State.opponent_moves:
                    pass
                else:
                    stderr.write('Unknown command: %s\n' % command)
                    stderr.flush()
            except EOFError:
                return

    def update_settings(self, options):
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        map_type = options[0]
        for i in range(1, len(options), 2):
            if map_type == State.setup_map_super_regions:
                self.setup_super_regions(options, i)
            elif map_type == State.setup_map_regions:
                self.setup_map_regions(options, i)
            elif map_type == State.setup_map_neighbours:
                self.setup_map_neighbours(options, i)
        if map_type == State.setup_map_neighbours:
            self.apply_map_neighbours()

    def setup_super_regions(self, options, i):
        super_region = SuperRegion(options[i], int(options[i + 1]))
        self.map.super_regions.append(super_region)

    def setup_map_regions(self, options, i):
        super_region = self.map.get_super_region_by_id(options[i + 1])
        region = Region(options[i], super_region)
        self.map.regions.append(region)
        super_region.regions.append(region)

    def setup_map_neighbours(self, options, i):
        region = self.map.get_region_by_id(options[i])
        neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]
        for neighbour in neighbours:
            region.neighbours.append(neighbour)
            neighbour.neighbours.append(region)

    def apply_map_neighbours(self):
        for region in self.map.regions:
            if region.is_on_super_region_border:
                continue
            for neighbour in region.neighbours:
                if neighbour.super_region.id != region.super_region.id:
                    region.is_on_super_region_border = True
                    neighbour.is_on_super_region_border = True

    def update_map(self, options):
        for i in range(0, len(options), 3):
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])
        self.round += 1

    def pick_starting_regions(self, options):
        picked = self.pickup.resolve(options)
        return ' '.join(picked)

    def place_troops(self):
        placements = self.deploy.resolve(self)
        return ', '.join(['%s %s %s %d' % (
            self.settings['your_bot'],
            State.go_place_armies,
            placement[0],
            placement[1]
        ) for placement in placements])

    def attack_transfer(self):
        attack_transfers = self.attack.resolve(self)
        if len(attack_transfers) == 0:
            return 'No moves'
        return ', '.join(['%s %s %s %s %s' % (
            self.settings['your_bot'],
            State.go_attack_transfer,
            attack_transfer[0],
            attack_transfer[1],
            attack_transfer[2]
        ) for attack_transfer in attack_transfers])


class Random(object):
    @staticmethod
    def randrange(_min, _max):
        return _min + int(fmod(pow(clock() + pi, 2), 1.0) * (_max - _min))

    @staticmethod
    def shuffle(items):
        i = len(items)
        while i > 1:
            i -= 1
            j = Random.randrange(0, i)
            items[j], items[i] = items[i], items[j]
        return items


class Region(object):
    def __init__(self, region_id, super_region):
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False


class SuperRegion(object):
    def __init__(self, super_region_id, worth):
        self.id = super_region_id
        self.worth = worth
        self.regions = []


class Map(object):
    def __init__(self):
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        return [region for region in self.regions if region.id == region_id][0]

    def get_super_region_by_id(self, super_region_id):
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        return [region for region in self.regions if region.owner == owner]


if __name__ == '__main__':
    Bot().run()
