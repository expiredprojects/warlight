import subprocess

import math
from bokeh.plotting import figure, output_file, save
from statistics import mean
import time
import shutil
import webbrowser
import os

from bot import Random


class Config(object):
    Build = 'scripts/./build.sh'
    Debug = 'scripts/./debug.sh'
    Run = 'scripts/./run.sh'
    Silent = 'scripts/./silent.sh'
    Output = 'out.txt'
    Snapshots = list([
        '001-random',
        '002-attack_only',
        '003-balanced_deploy',
        '004-defensive_deploy',
        '005-aggressive_deploy'
    ])


class Mode(object):
    Run = Config.Run
    Rounds = 10
    Snapshot = '004'
    Random = True


class Manager:
    class Search(object):
        Rounds = 'Rounds: '
        Winner = 'Winner: '
        Total = 'Total: '

    class Winner(object):
        Multiplier = dict(
            player=1.0,
            draw=0.5,
            enemy=-1.0
        )

    class Result(object):
        def __init__(self, rounds, winner, total):
            self.rounds = rounds
            self.winner = winner
            self.total = total
            self.average = float(total) / float(rounds)

    def __init__(self):
        self.rounds = Mode.Rounds
        self.results = list()

    @staticmethod
    def build():
        subprocess.call(Config.Build, shell=True)

    def parse_output(self):
        rounds, winner, total = None, None, None
        with open(Config.Output) as f:
            for line in f:
                if self.Search.Rounds in line:
                    raw = line.split(self.Search.Rounds)[1]
                    rounds = int(raw)
                elif self.Search.Winner in line:
                    raw = line.split(self.Search.Winner)[1]
                    winner = raw.strip('\n')
                elif self.Search.Total in line:
                    raw = line.split(self.Search.Total)[1]
                    total = int(raw)
        if rounds and winner and total:
            self.results.append(self.Result(rounds, winner, total))
        else:
            raise ValueError('Unable to parse output!')

    def run(self, snapshot):
        for _ in range(self.rounds):
            if snapshot:
                filename = '%s %s' % (Mode.Run, snapshot)
            else:
                snapshots = Config.Snapshots
                index = int(math.floor(Random.xrandrange(0, len(snapshots) - 1)))
                filename = '%s %s' % (Mode.Run, snapshots[index])
            subprocess.call(filename, shell=True)
            self.parse_output()

    def get_score(self, rounds_all, winners):
        score_all = list()
        for i in range(len(rounds_all)):
            multiplier = self.Winner.Multiplier.get(winners[i])
            score = (100 - rounds_all[i]) * multiplier
            score_all.append(score)
        return score_all

    @staticmethod
    def get_stats(winners):
        won = len([w for w in winners if w == 'player'])
        draw = len([w for w in winners if w == 'draw'])
        lost = len([w for w in winners if w == 'enemy'])
        return won, draw, lost

    def plot(self, annotate):
        # DECLARE
        winners = [r.winner for r in self.results]
        rounds_all = [r.rounds for r in self.results]
        range_rounds = range(self.rounds)
        score_all = self.get_score(rounds_all, winners)
        time_all = [r.total * 1/100 for r in self.results]
        average_all = [r.average for r in self.results]
        stats = self.get_stats(winners)
        final = mean(score_all)

        # PLOT
        now = int(time.time())
        if annotate:
            filename = '%s-%s.html' % (annotate, now)
        else:
            filename = 'index-%s.html' % now
        output_file(filename, title='Title')
        description = 'Won: %s Draw: %s Lost: %s Final Score: %s' % (stats[0], stats[1], stats[2], final)
        plot = figure(title=description, x_axis_label='x', y_axis_label='y')
        plot.line(range_rounds, score_all, legend='Score', line_dash='4 4', line_width=2)
        plot.circle(range_rounds, score_all, legend='Score', fill_color='white')
        plot.line(range_rounds, time_all, legend='Time', line_dash='4 4', line_color='red', line_width=2)
        plot.circle(range_rounds, time_all, legend='Time', fill_color='white', line_color='red')
        plot.line(range_rounds, average_all, legend='Time per Round', line_dash='4 4', line_color='orange', line_width=2)
        plot.circle(range_rounds, average_all, legend='Time per Round', fill_color='white', line_color='orange')

        # SAVE
        save(plot)
        shutil.move(filename, 'tests/%s' % filename)
        root = os.path.abspath(os.path.curdir)
        url = 'file://%s/tests/%s' % (root, filename)
        webbrowser.open(url, new=2)


if __name__ == '__main__':
    _annotate = raw_input('Annotate the project? (y/n): ')
    if _annotate == 'y':
        _annotate = raw_input('Annotate the project (lowercase): ')
    else:
        _annotate = None

    _snapshot = None
    if not Mode.Random:
        _snapshot = [Snapshot for Snapshot in Config.Snapshots if Mode.Snapshot in Snapshot]
        if _snapshot:
            _snapshot = _snapshot[0]

    manager = Manager()
    manager.build()
    manager.run(_snapshot)
    manager.plot(_annotate)
