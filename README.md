![alt tag](https://github.com/skylogic004/conquest-engine-gui/raw/master/screenshot.png)
------------------------------------------------------------

Python managing process:

    python manager.py

Build:

    bash scripts/build.sh

Debug:

    bash scripts/debug.sh

Run:

    bash scripts/run.sh

Silent Run:

    bash scripts/silent.sh

